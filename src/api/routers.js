import express from "express";
import {artists, albums} from "../db/dao.js";

function errorHandler (err, req, res, next) {
    if (res.headersSent) {
        return next(err);
    }
    res.status(500),
    res.render("error", { error: err });
}

class RouterBuilder{
    constructor(dao){
        this.dao = dao;
    }

    async getAll(req, res){
        const result = await this.dao.getAll();
        res.json(result);
    }

    async getOne(req, res){
        const result = await this.dao.getOne(req.params.id);
        if(result.rows.length === 0){
            return res.status(404).send();
        }
        res.json(result.rows);
    }

    async post(req, res){
        const result = await this.dao.create(req.body);
        res.json(result);
    }

    async update(req, res){
        const result = await this.dao.update(req.params.id, req.body);
        if(result.rows.length === 0){
            return res.status(404).send();
        }
        res.json(result.rows);
    }

    async delete(req, res){
        const result = await this.dao.remove(req.params.id);
        if(result.rows.length === 0){
            return res.status(404).send();
        }
        res.json(result.rows);
    }

    buildRouter(){
        const router = express.Router();
        router.use(express.json());
        const that = this;
        router.get("/", (req, res) => {
            that.getAll.call(that, req, res);
        });
        router.get("/:id", (req, res) => {
            that.getOne.call(that, req, res);
        });
        router.post("/", (req, res) => {
            that.post.call(that, req, res);
        });
        router.put("/:id", (req, res) => {
            that.update.call(that, req, res);
        });
        router.delete("/:id", (req, res) => {
            that.delete.call(that, req, res);
        });
        router.use(errorHandler);
        return router;
    }
}

class ArtistRouterBuilder extends RouterBuilder{
    constructor(){
        super(artists);
    }
}

class AlbumRouterBuilder extends RouterBuilder{
    constructor(){
        super(albums);
    }

    async getAll(req, res){
        const {from, to} = req.query;
        const result = await this.dao.getAll({from, to});
        res.json(result);
    }
}

const artistRouterBuilder = new ArtistRouterBuilder();
const albumRouterBuilder = new AlbumRouterBuilder();

const artistRouter = artistRouterBuilder.buildRouter();
const albumsRouter = albumRouterBuilder.buildRouter();

export {
    artistRouter,
    albumsRouter
};