import express from "express";
import {init} from "../db/dao.js";
import { artistRouter, albumsRouter } from "./routers.js";

function errorHandler (err, req, res, next) {
    if (res.headersSent) {
        return next(err);
    }
    res.status(500),
    res.render("error", { error: err });
}

const server = express();
server.use("/artists", artistRouter);
server.use("/albums", albumsRouter);
server.use(errorHandler);
init();

export default server;