import pg from "pg";
import queries from "./queries.js";

const isProd = process.env.NODE_ENV === "production";
const {
    PG_HOST, PG_USER, PG_PASSWORD,
    PG_DB, PG_PORT
} = process.env;

const pool = new pg.Pool({
    host: PG_HOST,
    user: PG_USER,
    password: PG_PASSWORD,
    database: PG_DB,
    port: PG_PORT,
    ssl: isProd
});

async function executeQuery(queryName, query, params){
    const client = await pool.connect();
    try{
        console.log(`Executing query: ${queryName}`);
        const result = await client.query(query, params);
        console.log(`Query: ${queryName} executed succesfully.`);
        return result;
    }catch(error){
        console.log(error.stack);
        error.name = "dbError";
        throw error;
    }finally{
        client.release();
    }
}

async function createTables(){
    await executeQuery("create tables", queries.createTables);
}

export {pool, executeQuery, createTables};
