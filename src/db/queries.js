export default {
    createTables: `
        CREATE TABLE IF NOT EXISTS "artists" (
            "id" SERIAL PRIMARY KEY,
            "name" VARCHAR UNIQUE NOT NULL
        );

        CREATE TABLE IF NOT EXISTS "albums" (
            "id" SERIAL PRIMARY KEY,
            "name" VARCHAR UNIQUE NOT NULL,
            "artist" INT NOT NULL,
            "release_date" smallint NOT NULL,
            CONSTRAINT fk_artist
                FOREIGN KEY("artist")
                    REFERENCES "artists"("id")
        );
    `,
    artists: {
        getAll: `
            SELECT * FROM artists;
        `,
        getOne: `
            SELECT * FROM artists
            WHERE id = $1::integer;
        `,
        create: `
            INSERT INTO artists (name)
            VALUES ($1);
        `,
        update: `
            UPDATE artists
            SET name=$1
            WHERE id=$2;
        `,
        delete: `
            DELETE FROM artists
            WHERE id=$1;
        `
    },
    albums: {
        getAll: `
            SELECT AL.id, AL.name, AR.name AS artist, AL.release_year
            FROM albums AS AL
            JOIN artists as AR ON
            AL.id = AR.id;
        `,
        getFrom: `
            SELECT AL.id, AL.name, AR.name AS artist, AL.release_year
            FROM albums AS AL
            JOIN artists as AR ON
            AL.id = AR.id
            WHERE AL.release_year >= $1;
        `,
        getTo: `
            SELECT AL.id, AL.name, AR.name AS artist, AL.release_year
            FROM albums AS AL
            JOIN artists as AR ON
            AL.id = AR.id
            WHERE AL.release_year <= $1;
        `,
        getFromTo: `
            SELECT AL.id, AL.name, AR.name AS artist, AL.release_year
            FROM albums AS AL
            JOIN artists as AR ON
            AL.id = AR.id
            WHERE AL.release_year >= $1 AND AL.release_year <= $2;
        `,
        getOne: `
            SELECT AL.id, AL.name, AR.name, AL.release_year
            FROM albums AS AL
            JOIN artists as AR ON
            AL.id = AR.id
            WHERE AL.id = $1;
        `,
        create: `
            INSERT INTO albums (name, artist, release_year)
            VALUES ($1, $2, $3);
        `,
        update: `
            UPDATE albums
            SET name=$1, artist=$2, release_year=$3
            WHERE id=$4;
        `,
        delete: `
            DELETE FROM albums
            WHERE id=$1;
        `
    }
};