import queries from "./queries.js";
import { executeQuery } from "./db.js";

class Resource{
    constructor(tableName, queries){
        this.tableName = tableName;
        this.queries = queries;
    }

    async getAll(){
        const result = await executeQuery(`Get all ${this.tableName}`, this.queries.getAll);
        return result.rows;
    }

    async getOne(id){
        const result = await executeQuery(`Getting ${this.tableName} with id ${id}`, this.queries.getOne, [id]);
        return result.rows;
    }

    async create({name}){
        const result = await executeQuery(`Create ${this.tableName}`, this.queries.create, [name]);
        return {name};
    }

    async update(id, {name}){
        const result = await executeQuery(`Update ${this.tableName}`, this.queries.update, [name, id]);
        return result;
    }

    async remove(id){
        const result = await executeQuery(`Removing ${this.tableName}`, this.queries.delete, [id]);
        return result;
    }
}

class Albums extends Resource{
    async getAll({from, to}){
        let result;
        if(from !== undefined && to !== undefined){
            result = await executeQuery(`Get albums from ${from} to ${to}`, this.queries.getFromTo, [from, to]);
        }else if(from !== undefined){
            result = await executeQuery(`Get albums from ${from} on`, this.queries.getFrom, [from]);
        }else if(to !== undefined){
            result = await executeQuery(`Get albums up to ${to}`, this.queries.getTo, [to]);
        }else{
            result = super.getAll();
        }
        return result;
    }

    async update(id, {name, artist, release_year: releaseYear}){
        const result = await executeQuery(`Update ${this.tableName}`, this.queries.update, [name, artist, releaseYear, id]);
        return result;
    }

    async create({name, artist, release_year}){
        const result = await executeQuery(`Create ${this.tableName}`, this.queries.create, [name, artist, release_year]);
        return {
            name, artist, release_year
        };
    }
}

async function init(){
    if(process.env.NODE_ENV !== "test"){
        const result = await executeQuery("Init tables", queries.createTables);
        return result.rows;
    }
}
const artists = new Resource("artists", queries.artists);
const albums = new Albums("albums", queries.albums);

export {
    artists, albums, init
};