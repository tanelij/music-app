import server from "./api/server.js";
const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
    const vars = [
        "PG_HOST", 
        "PG_USER",
        "PG_PASSWORD",
        "PG_DB",
        "PG_PORT",
        "PORT"
    ].map(v => `${v}: ${process.env[v]}`);

    console.log(`Server listening to port${PORT}`);
    console.log("Environment vars:", vars);
});