import request from "supertest";
import {jest} from "@jest/globals";
import server from "../../src/api/server.js";
import {pool} from "../../src/db/db.js";

const mockResponses = {
    artists: {
        rows: [
            {
                id: 1,
                name: "Juice Leskinen"
            },
            {
                id: 2,
                name: "Elvis Presley"
            }
        ]
    },
    albums: {
        rows: [
            {
                id: 1,
                name: "Taivaan Kappaleita",
                artist: 1,
                release_year: 1991
            },
            {
                id: 2,
                name: "Elvis is Back!",
                artist: 2,
                release_year: 1958
            }
        ]
    }
};

function initMock(expectedResponse){
    pool.connect = jest.fn(() => {
        return {
            query: () => {
                return expectedResponse;
            },
            release: () => null
        };
    });
}

describe("Testing /artists", () => {
    beforeAll(() => {
        initMock(mockResponses.artists);
    });
    afterEach(() => {
        jest.clearAllMocks();
    });
    it("Should return 200 and all artists on get /", async () => {
        const response = await request(server)
            .get("/artists")
            .set("Content-type", "application/json");

        expect(response.status).toBe(200);
        expect(response.body).toStrictEqual(mockResponses.artists.rows);
    });

    it("Should return 200 and the object on request body on post /", async () => {
        const artist = {
            name: "Tennis"
        };
        const response = await request(server)
            .post("/artists")
            .set("Content-type", "application/json")
            .send(artist);

        expect(response.status).toBe(200);
        expect(response.body).toStrictEqual(artist);
    });
});

describe("Testing /albums", () => {
    beforeAll(() => {
        initMock(mockResponses.albums);
    });
    afterEach(() => {
        jest.clearAllMocks();
    });
    it("Should return 200 and all albums on get /", async () => {
        const response = await request(server)
            .get("/albums")
            .set("Content-type", "application/json");

        expect(response.status).toBe(200);
        expect(response.body).toStrictEqual(mockResponses.albums.rows);
    });

    it("Should return 200 and the object on request body on post /", async () => {
        const album = {
            name: "Test",
            artist: 1,
            release_year: 1989
        };
        const response = await request(server)
            .post("/albums")
            .set("Content-type", "application/json")
            .send(album);

        expect(response.status).toBe(200);
        expect(response.body).toStrictEqual(album);
    });
});